---?image=assets/puppet-heart-dag.jpg&size=contain&color=black
@title[Puppet for Linux]
## Puppet for Linux
##### <span style="font-family:Roboto Condensed; font-weight:bold">Gaining linux superpowers with puppet enterprise</span>

---?color=black
@title[Who am I]

## Who am I
<span style="font-size:0.6em; color:gray">William Rodriguez</span> |
<span style="font-size:0.6em; color:gray">I'm a Puppet Certified Professional who has been working with puppet for over three years.</span>

@fa[arrow-right]

---?color=black
@title[About this Presentation]

## About this presentation

<p style="font-size:0.7em; line-height:1em">This presentation is aimed towards using Puppet Enterprise. You can find this presentation, plus the examples and solutions in the gitlab project at [wil.rodriguez/puppet-training](https://gitlab.com/wil.rodriguez/puppet-training)</p>

#### References
<ul style="font-size:0.7em; line-height:1em">
  <li><a href="https://puppet.com/docs">Puppet Docs</a> - Puppet's docs are the best place to learn about Puppet's advanced behaviors</li>
  <li><a href="https://www.example42.com/blog/">Example42 Blog</a> - Example42 is one of the premiere community contributors</li>
</ul>

---?image=https://raw.githubusercontent.com/gitpitch/the-template/master/template/img/bg/orange.jpg&position=right&size=50% 100%&color=black
@title[Day 1 Agenda]

@snap[west text-16 text-bold text-italic text-orange span-50]
Day 1
@snapend

@snap[east text-white span-45]
@ol[split-screen-list text-08](false)
- Introduction to puppet
- Lab Setup
- Learning the Puppet CLI
- Writing Puppet DSL (Part 1)
- Using Forge Modules
@olend
@snapend

@snap[south-west template-note text-gray]
Start: 9ish, Finish: 4:30ish, Lunch TBD
@snapend

+++?image=https://raw.githubusercontent.com/gitpitch/the-template/master/template/img/bg/blue.jpg&position=right&size=50% 100%&color=black
@title[Day 2 Agenda]

@snap[west text-16 text-bold text-italic text-blue span-50]
Day 2
@snapend

@snap[east text-white span-45]
@ol[split-screen-list text-08](false)
- Hiera
- Writing Puppet DSL (Part 2)
- Working with files
- More Puppet DSL
- Advanced Puppet Syntax
@olend
@snapend

@snap[south-west template-note text-gray]
Start: 9ish, Finish: 4:30ish, Lunch TBD
@snapend

+++?image=https://raw.githubusercontent.com/gitpitch/the-template/master/template/img/bg/purple.jpg&position=right&size=50% 100%&color=black
@title[Day 3 Agenda]

@snap[west text-16 text-bold text-italic text-purple span-50]
Day 3
@snapend

@snap[east text-white span-45]
@ol[split-screen-list text-08](false)
- Writing Templates
- Using the exec resource
- Writing facts
- Writing tasks
- Writing plans
@olend
@snapend

@snap[south-west template-note text-gray]
Start: 9ish, Finish: 4:30ish, Lunch TBD
@snapend

---?color=black

@title[What is Puppet]

@snap[west span-40]
## What is Puppet
@snapend

@snap[south-west template-note text-white]
Section Overview
@snapend

@snap[north-east span-60]
@box[bg-purple text-white](Configuration Management#A cross-platform tool that runs on Windows/&ast;nix/Mac, and more.)
@snapend

@snap[east span-60]
@box[bg-orange text-white](Puppet Desired State Language (DSL)#Model the intended **state** of a system, not how to get there.)
@snapend

@snap[south-east span-60]
@box[bg-pink text-white](Infrastructure as Code#Develop, test, and deploy your infrastructure with traditional code management workflows)
@snapend

+++?color=black

### Puppet Overview

![Youtube Video](https://www.youtube.com/embed/QFcqvBk1gNA)

+++?color=black

@title[Configuration Management]

@snap[west span-40]
### Configuration Management
@snapend

@snap[north-east span-60 fragment]
@box[bg-purple text-white](Constant Enforcement#Prevent drift with constant and consistent enforcement.)
@snapend

@snap[east span-60 fragment]
@box[bg-orange text-white](Aggregate Data#All puppet data is automatally stored into puppetdb for easy reporting.)
@snapend

@snap[south-east span-60 fragment]
@box[bg-pink text-white](Build Safely#Preview changes in noop mode before they go live.)
@snapend

+++?color=black

@title[The Puppet DSL]

@snap[west span-40]
### Puppet DSL
@snapend

@snap[north-east span-60 fragment]
@box[bg-purple text-white](Declarative#Focused on modeling **desired state**, not procedure.)
@snapend

@snap[east span-60 fragment]
@box[bg-orange text-white](Resource-based#Abstracts system resources to generalize system configurations.)
@snapend

@snap[south-east span-60 fragment]
@box[bg-pink text-white](Idempotent#Can be applied repeatedly and will always yield the same result.)
@snapend

+++?color=black

@title[Infrastructure as Code]

@snap[west span-40]
### Infrastructure as Code
@snapend

@snap[north-east span-60 fragment]
@box[bg-purple text-white](Development Tooling#Common toolchains speed up development, testing and deploy.)
@snapend

@snap[east span-60 fragment]
@box[bg-orange text-white](Code Reuse#By abstracting configurations, you can reuse your code and scale faster.)
@snapend

@snap[south-east span-60 fragment]
@box[bg-pink text-white](Single Source of Truth#Build a single source of truth that defines your whole stack.)
@snapend

@snap[south-west template-note text-white]
@fa[arrow-right]
@snapend

---?color=black
@title[Lab 0: Lab Provisioning]

@snap[north-west span-40]
# Lab 0
@snapend

@snap[north-east span-60]
@box[bg-purple text-white](Lab Provisioning#Follow the lab guide to deploy your systems. You'll be provisioning two systema, a master and a client.)
@snapend

+++?color=black
@title[Lab 1: Lab Setup]

@snap[north-west span-40]
# Lab 1
@snapend

@snap[north-east span-60]
@box[bg-purple text-white](Lab Setup#Get the system ready for our lab and then install puppet enterprise.)
@snapend

@snap[east span-60]
@box[bg-orange text-white](Control-repo Setup#Create a control-repo and set up PE to use it.)
@snapend

@snap[south-east span-60]
@box[bg-pink text-white](Installing the Puppet Agent#Install the puppet agent on our second node.)
@snapend

---?color=black

@title[Puppet At A Glance]

@snap[north-west span-40]
## Puppet At A Glance
@snapend

@snap[south-west template-note text-white]
Section overview
@snapend

@snap[north-east span-60]
@box[bg-purple text-white](Puppet Resources#The building blocks of the Puppet DSL.)
@snapend

@snap[east span-60]
@box[bg-orange text-white](The Puppet Agent#The thing that actually applies your puppet code.)
@snapend

@snap[south-east span-60]
@box[bg-pink text-white](Facter#Facter provides us information about our systems.)
@snapend

+++?color=black

@title[Puppet Resources]

### Puppet Resources

@css[text-05 text-left](You can think of resources as objects in other languages. They represent a state of a particular **type** of *thing* on the system you're managing. There are many types such as `file`, `service`, `package`, `user`, and many more.)

@css[text-05 text-left](Each resource type has its own attributes that allows it to describe the state of an instance of that particular resource type on the system. For instance, a file may look something like this:)

``` puppet
file { '/home/bob/myfile':
  ensure  => file,
  content => "Hi I'm bob",
  owner   => 'bob',
  group   => 'bob'
}
```

@[1](This is the **type** and **title** of the resource. This is a file type resource, with the title '/home/bob/myfile')
@[2-5](These are parameters. Each resource type has its own unique set of parameters that describe it.)

+++?color=black
@title[Puppet]
@snap[north-west]

### Puppet

@snapend

@snap[west text-05 text-left list-content-concise span-100]
The puppet component of the agent is the primary component, it has two modes, both usable via command line:
@ul[list-content-concise](false)

- @color[green](`puppet agent`) &mdash; Used in client/server configuration. Collects information about the node and sends it to the master, then applies the catalog the master sends back.</li>
- @color[green](`puppet apply`) &mdash; Used in client-only configuration. Collects information about the node and then compiles its own catalog using code the user provides.</li>

@ulend
You can read more in the [docs](https://puppet.com/docs/puppet/5.5/man/index.html)
@snapend

+++?color=black

@title[The Agent Lifecycle]

### The Agent Lifecycle

![Agent Lifecycle](https://image.slidesharecdn.com/puppet-overview-131204211724-phpapp02/95/puppet-overview-12-638.jpg)

+++?color=black
@title[Facter]

### Facter

<p style="font-size:0.8em; line-height:1em">Facter is a simple utility that fathers information about a system, **facts**, such as cpu, memory, ipaddresses, etc... Puppet runs facter at the beginning of every run and provides those values as top-scoped variables in your puppet code.</p>

<p style="font-size:0.8em; line-height:1em">Facter is also available at the command line with the @color[green](`facter`) command.</p>

<p style="font-size:0.8em; line-height:1em">The core facts provided by facter can be extended with custom facts written in ruby or external facts written in another language, or even flat files.</p>

+++?color=black
@title[Pxp-agent]
### PXP Agent
The pxp-agent is a core component of the Puppet Enterprise workflow. It is the backbone of the orchestration layer and uses PCP (Puppet Communication Protocol) as a transport for application orchestration, jobs, and tasks. It is only utilized in Puppet Enterprise and is not really used in Puppet Open Source.

---?color=black
@title[Lab 2: The Puppet CLI]

@snap[north-west span-40]
# Lab 2
@snapend

@snap[north-east span-60]
@box[bg-purple text-white](Learning the Puppet CLI#In this lab you'll work with some common Puppet CLI commands.)
@snapend

@snap[east span-60 text-06]
You'll be working with the following commands:
@ol[list-content-concise](false)
- `puppet describe`
- `puppet resource`
- `puppet apply`
- `puppet agent`
- `facter`
@olend
@snapend

---?color=black
@title[The Puppet Language: Resources]

@snap[north-west span-40]

## The Puppet Language: Resources

@snapend

@snap[south-west template-note text-white]
Section overview
@snapend

@snap[north-east span-60]
@box[bg-purple text-white](All About Resources#What is a resource and what does it look like)
@snapend

@snap[east span-60]
@box[bg-orange text-white](Resource Ordering#How to make sure everything happens in the right order)
@snapend

@snap[south-east span-60]
@box[bg-pink text-white](Common Resources#Some of the most common resource types you'll be working with)
@snapend

+++?color=black

@title[All About Resources: Syntax]

### All About Resources: Syntax

[Resources](https://puppet.com/docs/puppet/5.4/lang_resources.html) are the most
basic unit for modeling the intended state of something on a system, such as a
service or a package. In general the syntax for a resource looks like this:

``` puppet
<TYPE> { '<TITLE>':
  <ATTRIBUTE> => <VALUE>,
}
```

+++?color=black
@title[All About Resources: Types]

### All About Resources: Types

Resource types are generalized models that abstract common units of configuration on a system such as @color[green](`package`), @color[green](`service`), @color[green](`file`), and many others. There's a handy [cheat sheet](https://puppet.com/docs/puppet/5.5/cheatsheet_core_types.html) of the core types in the puppet docs

For more information on the resources listed here as well as a full list of available respources, you can check the [docs](https://puppet.com/docs/puppet/5.4/type.html).

+++?color=black
@title[All About Resources: Uniqueness]

### All About Resources: Uniqueness

<p style="font-size:0.8em; line-height:1em">Resources must be unique. You can only define a resource with a specific type and title once. Compilation will fail if a resource is declared twice.</p>

``` puppet
package { 'httpd': ensure => present, }
service { 'httpd': ensure => running, }
```

<p style="font-size:0.8em; line-height:1em; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">@fa[check-circle fa-green] This is fine.</p>

``` puppet
package { 'httpd': ensure => installed, }
package { 'httpd': ensure => present, }
```

<p style="font-size:0.8em; line-height:1em; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">@fa[exclamation-circle fa-red] This is not fine.</p>
@fa[arrow-down]

+++?color=black
@title[Resource Ordering: Overview]

### Resource Ordering: Overview

Ordering is important in puppet. There are metaparameters to allow you to order
your resources.

``` puppet
package { 'httpd': ensure => present }
service { 'httpd':
  ensure  => running,
  enable  => true,
  require => Package['httpd'],
}
```

+++?color=black
@title[Resource Ordering: Metaparameters]

### Resource Ordering: Metaparameters

These metaparameters allow for basic ordering

- `require` - *The required resource* will be applied before this resource.
- `before` - *This resource* will be applied before the dependent resource.

+++?color=black
@title[Resource Ordering: Events]

### Resource Ordering: Events

Puppet can also create events when resources change
<ul style="font-size:0.7em; line-height:1.5em">
  <li>`notify` - *This resource* will be applied before the dependent resource. AND
    if puppet changes *this resource*, it will send a refresh event to the
    dependent resource</li>
  <li>`subscribe` - *The required resource* will be applied before this resource. AND
    if puppet changes *any subscribed resources*, it will cause this resource to
    refresh</li>
</ul>

+++?color=black
@title[Common Resources: File]

### Common Resources: File

[Link to docs](https://puppet.com/docs/puppet/latest/types/file.html)

```puppet
file { '/etc/odbcinst.ini':
  ensure  => file,
  source  => 'puppet:///modules/profile/aml/odbcinst.ini',
  owner   => 'root',
  group   => 'aml',
  mode    => '0664',
  require => Package['unixODBC'],
}
```

@[2](File resources support three main ensure values: file, directory, and link.)
@[3](The source attribute allows you to deploy a file to the system.)
@[4-6](The file type also supports setting standard file properties.)
@[6](Note that mode should always be specified as a string with leading zero. Symbolic modes are also supported, but not encouraged)
@[7](Behold, a metaparameter in the wild.)

+++?color=black
@title[Common Resources: Package]

### Common Resources: Package

[Link to docs](https://puppet.com/docs/puppet/latest/types/package.html)

```puppet
package { 'unixODBC':
  ensure => installed,
}
```

@[1](There's really not all that much to this one.)
@[2](The ensure attribute accepts: installed, purged, and a specific version number)

+++?color=black
@title[Common Resources: Service]

### Common Resources: Service

[Link to docs](https://puppet.com/docs/puppet/latest/types/service.html)

```puppet
service { 'NetworkManager':
  ensure => running,
  enable => true,
}
```

@[1](There's really not all that much to this one either.)
@[2](The ensure attribute accepts: running and stopped.)
@[3](The enable attribute sets whether the service should run at boot, it accepts true or false.)

+++?color=black
@title[Common Resources: Service]

### Common Resources: Exec

[Link to docs](https://puppet.com/docs/puppet/latest/types/exec.html)

```puppet
exec { 'Register to satellite':
  command   => "subscription-manager register --org='rhel' --activationkey=\"${satelliteactivationkey}\"",
  onlyif    => 'facter rhsm.identity.registered | egrep -q "(Unknown|false)"',
  provider  => 'shell',
  returns   => [0, 1, 64, 70],
  logoutput => true,
  path      => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/opt/puppetlabs/bin',
  require   => Exec["katello-ca-consumer-${satellitecapsule} install"],
  notify    => Ini_setting['Force refresh rhsm facts'],
}
```

@[1](Puppet resources not cutting it? Meet `exec`, the true workhorse of every sufficiently complex manifest.)
@[1](The title is usually used as just a comment about what exec does.)
@[2](The main command is specified via the command attribute. By default, exec is not idempotent and will execute this command every. single. run.)
@[3](But, we have four attributes to make it idempotent: creates, refreshonly, onlyif, and unless.)
@[4](Also note that by default, exec resources are invoked directly, if you use the shell provider, it'll spawn it with `/bin/sh` instead.)
@[5](Chances are, your command gives some weird exit codes, you can make puppet accept them all.)
@[7](You should either specify a path or fully qualify your commands, unless you don't want your code to work)

---?color=black

@title[The Puppet Language: Classes]

## The Puppet Language: Classes

[Classes](https://puppet.com/docs/puppet/5.5/lang_classes.html) are named blocks of puppet code stored in modules for repeated use. They won't be applied until called by name in some fashion and won't be added to the catalog unless either declared in another manifest or assigned in the classifier.

+++?color=black
@title[Classes: Syntax]

### Classes: Syntax

``` puppet
class <NAME> (
  <DATATYPE> <PARAMETER> = <VALUE>
) inherits <PARENT_NAME> {
    <CODE>
}
```

<ul style="font-size:0.8em; line-height:1em; color:gray;">
  <li>The `name` must follow certain [rules](https://puppet.com/docs/puppet/5.4/lang_reserved.html#classes-and-defined-resource-types)</li>
  <li>The parameters section as well as its surrounding parenthesis are optional. Parameters also do not need to be set to value, this is only for the default value and can be overridden either when the class is called via automatic parameter lookup.</li>
  <li>You can have many parameters but they must be on separate lines with commas at the end of each line. The comma is optional on the last parameter line.</li>
  <li>The `inherits` keyword and the parent name that follows are both optional and allow your class to extend another class, bringing that class's variables and parameters into the current scope. The puppet style guide discourages using inheritance.</li>
</ul>

+++?color=black
@title[Classes: Usage]

### Classes: Usage

There's three main ways to call a class:

- Classification - Using the PE console or another external node classifier you can apply a class
- @color[green](`include`) statement - the include statement can be repeated as many times as you want.
- Resource-like statement - This statement can only be called once, the same as any other resource.

``` puppet
class { 'myclass':
  myparam => 'test',
}
```

---?color=black
@title[Lab 3: Our First Puppet Profile]

@snap[north-west span-40]

# Lab 3

@snapend

@snap[north-east span-60]
@box[bg-purple text-white](Our First Puppet Profile#We'll be writing a puppet profile that installs some packages.)
@snapend

@snap[east span-60 text-06]
You'll be installing these packages:

@ol[list-content-concise](false)

- `tree`
- `zsh`
- `puppet-bolt`
- `git2u`
- `python36u`

@olend
@snapend

---?color=black
@title[Lab 4: Using Forge Modules]

@snap[north-west span-40]

# Lab 4

@snapend

@snap[north-east span-60]
@box[bg-purple text-white](Using Puppet Forge Modules#We're going to implement two community puppet modules.)
@snapend

@snap[east span-60 text-06]
Look for modules to solve two problems:

@ol[list-content-concise](false)

- Keeping NTP in sync
- Setting unique bash prompts for each of your systems.

@olend
@snapend

---?color=black
@title[Modules]

@snap[west span-40]

## Modules

@snapend

@snap[south-west template-note text-white]
Section Overview
@snapend

@snap[north-east span-60]
@box[bg-purple text-white](About Modules#What are modules? What do they look like, and what are they for?)
@snapend

@snap[east span-60]
@box[bg-orange text-white](The Puppet Forge#Learning how to use modules from the community.)
@snapend

@snap[south-east span-60]
@box[bg-pink text-white](Hiera#Customizing the behaviour of our code, without changing our code.)
@snapend

+++?color=black
@title[About Modules]

@snap[north-east span-40]

## About Modules

@snapend

@snap[east span-40]
@box[bg-green text-white](Modules are a way of organizing puppet code, intended to be portable and reusable.)
@snapend

```bash
ntp
├── data
│   └── ...
├── examples
│   └── ...
├── files
│   └── ...
├── manifests
│   ├── init.pp
│   └── ...
├── templates
│   └── ...
├── hiera.yaml
├── metadata.json
└── README.md
```

@[1](Here we have the basic skeleton for a module called ntp)
@[2-12](Modules are made up of a number of directories. We see only a few of the most common here. We'll cover them in order of importance.)
@[8-10](Manifests is where your puppet code goes. Note that there can be a special file here called `init.pp` which can contain a class named after the module)
@[6-7](Whatever you put into the files directory will be made available in your puppet runs to be deployed to your system using the `file` resource.)
@[11-12](The templates directory allows you to store erb and epp templates you can also utilize via the `file` resource.)
@[2-3](The data directory is the place where you put your hieradata. Note, we'll talk more about that in a moment.)
@[13](The hiera.yaml file governs the hierarchy of the module.)
@[14](The metadata.json file contains metadata about the module, including version, description, dependencies, and compatibility.)
@[15](It's a readme.)

+++?color=black
@title[The Puppet Forge]

## The Puppet Forge

The [puppet forge](https://forge.puppet.com) contains over 4600 community developed puppet modules that extend
the built-in functionality of puppet. You can find modules to configure ntp, ssh, bash prompts, sysctl, and
a myriad of other software and technologies.

Using modules from the forge is usually a matter of just reading their documentation, but it's often as simple as:

``` puppet
include ntp
```

+++?color=black
@title[Hiera]
@snap[north-east span-40]

## Hiera

@snapend

@snap[east span-40]
@box[bg-green text-white text-06](Hiera gives you a hierarchy that you can define in hiera.yaml. You can then have puppet look up a key in the hierarchy.)
@snapend

```yaml
version: 5
defaults:
  datadir: data
hierarchy:
  - name: "Normal data"
    data_hash: yaml_data
    paths:
      - "nodes/%{trusted.certname}.yaml"
      - "common.yaml"
```

@[4-9](This is the hierarchy of files puppet will read. You can define multiple levels of the hierarchy and puppet will descend through each.)
@[8](Each path in the hierarchy can be interpolated with facts from the system that make the level unique and specific.)
@[6](Hiera supports multiple backends such as yaml, json, and the eyaml backend, which allows sensitive data to be encrypted.)
@[3](The standard directory for hieradata is the data directory, but you can change this setting here.)
@[1](We currently use hiera 5)

---?color=black
@title[Lab 4: Using Forge Modules]

@snap[north-west span-40]

# Lab 5

@snapend

@snap[north-east span-60]
@box[bg-purple text-white](Using Hiera#We're going to use hiera to set some parameters for your two forge puppet modules.)
@snapend

@snap[east span-60 text-06]
Use hiera to set your parameters for:

@ol[list-content-concise](false)

- ntp
- colorprompt

@olend
@snapend

---?color=black
@title[Conditional Logic]

@snap[west span-40]

## Conditional Logic

@snapend

@snap[south-west template-note text-white]
Section Overview
@snapend

@snap[north-east span-60]
@box[bg-purple text-white](If/Unless#Old faithful vs unholy abomination)
@snapend

@snap[east span-60]
@box[bg-orange text-white](Cases#For when if statements don't do the job)
@snapend

@snap[south-east span-60]
@box[bg-pink text-white](Selectors#The new hotness)
@snapend

+++?color=black
@title[If/Unless]
@snap[north-east span-40]

## If/Unless

@snapend

@snap[east span-40]
@box[bg-green text-white text-06](If statements behave exactly how you'd expect that they would. Unless statements do too, if you *know* what to expect from them.)
@snapend

```puppet
if $facts['is_virtual'] {
  notice('This is a VM')
}
elsif $facts['osname'] == 'Darwin' {
  warning('Turns out this is a mac')
}
else {
  include ntp
}

unless $facts['memory']['system']['totalbytes'] > 1073741824 {
  $maxclient = 500
}
```

+++?color=black
@title[Cases]
@snap[north-east span-40]

## Cases

@box[bg-green text-white text-06](Cases basically behave more or less like they do in every other language. They do, however, have intrinsic `in` and regex support.)
@snapend

```puppet
case $facts['os']['name'] {
  'Solaris':           { include role::solaris }
  'RedHat', 'CentOS':  { include role::redhat  }
  /^(Debian|Ubuntu)$/: { include role::debian  }
  default:             { include role::generic }
}
```

@snap[south-east template-note text-white]
[More on case matching](https://puppet.com/docs/puppet/6.3/lang_conditional.html#case-matching)
@snapend

+++?color=black
@title[Selectors]
@snap[north-east span-40]

## Selectors

@box[bg-green text-white text-06](Selectors are a lot like a combination of a case and ternary. They yield a value, based on cases.)
@snapend

```puppet
$system = $facts['os']['name'] ? {
  /(RedHat|Debian)/ => "our system is ${1}",
  default           => "our system is unknown",
}
```

@snap[south-east template-note text-white]
[More on case matching](https://puppet.com/docs/puppet/6.3/lang_conditional.html#case-matching-1)
@snapend

---?color=black
@title[Expressions and operators]

## Expressions and Operators

An [expression](https://puppet.com/docs/puppet/5.5/lang_expressions.html) is something that resolves to a value. In puppet, almost everything resolves to a value, including literal values, variable references, function calls, resource declarations, selectors and more.

As a result, almost anywhere you need to supply a value, you can use an expression.

+++?color=black
@title[What's not an expression]

### What's not an expression

These things aren't expressions:

- Class definitions
- Defined types
- Node definitions
- Resource collectors
- Lambdas

+++?color=black
@title[Where can you use expressions]

### Where can you use expressions

- The operand of another expression
- The condition of an [if statement](https://puppet.com/docs/puppet/6.2/lang_conditional.html)
- The control expression of a [case statement](https://puppet.com/docs/puppet/6.2/lang_conditional.html) or [selector statement](https://puppet.com/docs/puppet/6.2/lang_conditional.html)
- The assignment value of a variable
- The value of a resource attribute
- The argument or arguments of a function call
- The title of a resource
- An entry in an array or a key or value of a hash

+++?color=black
@title[Operators]

## Operators

- Any expression can optionally be surrounded by parentheses. This can change the order of evaluation in compound expressions (e.g. `10+10/5` is `12`, and `(10+10)/5` is `4`), or just make your code clearer.
- **Binary** (Infix) operators appear between two operands: `$a = 1`, `5 < 9`, `$operatingsystem != 'Solaris'`, etc.
- **Unary** (Prefix) operators appear immediately before a single operand: `*$interfaces`, `!$is_virtual`, etc.

+++?color=black
@title[Comparison Operators]
@snap[north-west text-06]
## Comparison Operators
@snapend

@snap[west span-40 text-07]
@box[text-07](Comparison Rules:)
@ul[list-content-concise text-06](false)
- They take operands of several data types
- They resolve to boolean values
@ulend
@snapend

@snap[midpoint span-40 text-07]
@box[text-07](The usual:)
@ul[list-content-concise text-06](false)
- `==` (equality)
- `!=` (non-equality)
- `<` (less than)
- `>` (greater than)
- `<=` (less than or equal to)
- `>=` (greater than or equal to)
@ulend
@snapend

+++?color=black
@title[Comparison Operators Continued]

@box[text-07](`=~` (Regex or Datatype Matching) and `!~` (Non-matching))
- A regular expression (regex), such as /^[<>=]{7}/.
- A stringified regular expression — that is, a string that represents a regular expression, such as "^[<>=]{7}".
- A data type, such as Integer[1,10].

---?color=black
@title[Defined Resource Types]

## Defined Resource Types

[Defined types](https://puppet.com/docs/puppet/5.5/lang_defined_types.html) are blocks of puppet code that can be evaluated multiple times with different parameters. They function like native resource types in many ways, even though they are typically made up of collections of resources. In the next slide we'll look at an example from the puppet docs.

+++?color=black
@title[Defined Types: Example]

### Defined Types: Example

``` puppet
define apache::vhost (
  Integer $port,
  String[1] $docroot,
  String[1] $servername = $title,
  String $vhost_name = '*',
  String $vhost_dir = $apache::params::vhost_dir
) {
  file { "${vhost_dir}/${servername}.conf":
    ensure  => file,
    owner   => 'www',
    group   => 'www',
    mode    => '0644',
    content => template('apache/vhost-default.conf.erb'),
    notify  => Service['httpd'],
  }
}
```

@[1](`define` keyword and standard classpath)
@[2-6](Parameters are similar to class parameters)
@[4](You get the `$title` variable for free)
@[8](Any resources in a defined type have to be made unique somehow.)
@[13](ERB templates can access all variables in our current scope, because magic.)

+++?color=black
@title[Using a defined type]

### Using a defined type

You declare a defined type the same way you'd declare any other resource. To declare a resource of the `apache::vhost` type from the last example:

``` puppet
apache::vhost {'homepages':
  port    => 8081,
  docroot => '/var/www-testhost',
}
```

---

@title[Puppet Tasks]

### Puppet Tasks

![Youtube Video](https://www.youtube.com/embed/wN0SQ1K8Rxk)
