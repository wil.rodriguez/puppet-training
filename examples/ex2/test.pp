$comment = 'I am Groot'

package { 'tree':
  ensure => installed,
}

user { 'root':
  ensure             => 'present',
  comment            => $comment,
  gid                => 0,
  home               => '/root',
  password_max_age   => -1,
  password_min_age   => 0,
  password_warn_days => 7,
  shell              => '/bin/bash',
  uid                => 0,
}
